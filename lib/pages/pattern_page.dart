import 'package:flutter/material.dart';
import 'package:word_wonders/application.dart';

class PatternPage extends StatefulWidget {
  final String word;
  PatternPage(this.word);
  _PatternPageState createState() => _PatternPageState();
}

class _PatternPageState extends State<PatternPage> {
  List<String> words;
  @override
  void initState() {
    _findWords();
    super.initState();
  }

  _findWords() async {
    words = [];
    String sql =
        "SELECT * FROM english WHERE word LIKE '" + widget.word.replaceAll("?", "_") + "' ORDER BY word;";
    List<Map> results = await app.db.rawQuery(sql);
    print(results.length);
    results.forEach((element) {
      words.add(element["word"]);
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.word),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20),
          child: _buildList(),
        ),
      ),
    );
  }

  _buildList() {
    if (words == null) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return ListView.builder(
      itemBuilder: (context, index) {
        return Container(
          padding: EdgeInsets.all(5),
          child: Text((index+1).toString() + ". " + words[index]),
        );
      },
      itemCount: words.length,
    );
  }
}
