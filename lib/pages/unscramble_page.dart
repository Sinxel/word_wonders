import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:word_wonders/widgets/words_list.dart';

class UnscramblePage extends StatefulWidget{
  final String word;
  UnscramblePage(this.word);
  _UnscramblePageState createState() => _UnscramblePageState();
}

class _UnscramblePageState extends State<UnscramblePage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.word),),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20,),
          child: ListView(
            children: _buildLists(),
          ),
        ),
      ),
    );
  }

  _buildLists(){
    List<Widget> children = [];
    for(int i=widget.word.length; i>=2; i--){
      children.add(_generateWordsWidget(i));
      children.add(SizedBox(height: 20,));
    }
    return children;
  }

  _generateWordsWidget(int numberOfLetters){
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10,),
      ),
      child: ExpansionTile(
        title: Text(numberOfLetters.toString()+" Letters words"),
        initiallyExpanded: true,
        expandedCrossAxisAlignment: CrossAxisAlignment.stretch,
        childrenPadding: EdgeInsets.all(10),
        children: [
          WordsList(numberOfLetters, widget.word),
        ],

      ),


    );
  }

}