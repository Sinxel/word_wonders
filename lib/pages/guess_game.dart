import 'package:flutter/material.dart';
import 'package:word_wonders/application.dart';
import 'package:word_wonders/models/letter_data.dart';
import 'package:word_wonders/widgets/in_app_keyboard.dart';

class GuessGamePage extends StatefulWidget{
  _GuessGameState createState() => _GuessGameState();
}

enum GuessGameStatus{
  INITIALIZING,
  READY,
  GAME_OVER,
  WIN
}
class _GuessGameState extends State<GuessGamePage>{
  GuessGameStatus _gameStatus = GuessGameStatus.INITIALIZING;
  String _word = "";
  List<String> _guesses = [];
  String _currentGuess = "";

  @override
  void initState() {
    _getWord();
    super.initState();

  }
  _getWord() async{
    String sql = "SELECT word FROM english WHERE length(word)=5 AND  word NOT IN (SELECT word FROM guessedWords) ORDER BY RANDOM() LIMIT 1;";
    List<Map> result  = await app.db.rawQuery(sql);
    setState(() {
      _word = result[0]['word'];
      _gameStatus = GuessGameStatus.READY;
    });
    print(_word);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Guess The Word"),
      ),
      body: _buildPage(),
    );
  }

  _buildPage(){
    if(_gameStatus==GuessGameStatus.INITIALIZING){
      return Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }
    List<Widget> children = [];
    for(int i=0; i<_guesses.length; i++){
      children.add(_wordWidget(_guesses[i]));
    }
    if(_gameStatus==GuessGameStatus.READY){
      children.add(_currentGuessWidget());
    }else{
      children.add(_buildGameMessage());
    }

    children.addAll( [Spacer(),
        _buildKeyboard()]);
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Column(
        children: children,
      ),
    );
  }

  _buildGameMessage(){
    return Column(
      children: [
        Text(_gameStatus.toString()),
        Text(_word.toUpperCase())
      ],
    );
  }
  _buildKeyboard(){
    return InAppKeyboard(keyPressed: _buttonPressed,);
  }

  _buttonPressed(String char){
    _addLetter(char);
  }

  _addLetter(String ch){
    if(_gameStatus!=GuessGameStatus.READY) return;
    if(ch=='ENTER'&&_currentGuess.length==5){
      //check correct letters;
      _guesses.add(_currentGuess);
      _currentGuess = "";
     _checkGameWon();
      return;
    }
    if(ch=='ENTER') return;
    if(ch=='DELETE'){
      if(_currentGuess.length==0) return;
      _currentGuess = _currentGuess.substring(0, _currentGuess.length-1);
      setState(() {

      });
      return;
    }
    if(_currentGuess.length<_word.length){
      _currentGuess += ch;
      setState(() {

      });
    }

  }
  _checkGameWon(){
    if(_currentGuess.toLowerCase()==_word.toLowerCase()){
      _gameStatus = GuessGameStatus.WIN;
    }else if(_guesses.length==5){
      _gameStatus = GuessGameStatus.GAME_OVER;
    }
    setState(() {

    });
  }
  _currentGuessWidget(){
    List<Widget> children = [];
    _currentGuess.characters.forEach((element) {
      children.add(_letterBox(new LetterData(content: element)));
    });
    int length = _word.length - _currentGuess.length;
    for(int i=0; i<length; i++){
      children.add(_letterBox(new LetterData()));
    }
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: children,
    );
  }
  _wordWidget(String word){
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: _buildLetterBoxes(word),
    );
  }

  _buildLetterBoxes(String word){
    
    List<Widget> boxes = [];
    int length = word.length>0?word.length:_word.length;

    for(int i=0; i<length; i++){
      String guessCharacter   = word.substring(i, i+1);
      String wordCharacter = _word.substring(i, i+1);
      LetterState state = LetterState.NONE;

      if(guessCharacter.toLowerCase()==wordCharacter.toLowerCase()) {
        state = LetterState.CORRECT;
      }else if(_word.toLowerCase().contains(guessCharacter.toLowerCase())){
        state = LetterState.WRONG_POSITION;
      }else{
        state = LetterState.WRONG;
      }

      boxes.add(_letterBox(new LetterData(content: word.substring(i, i+1), state: state)));

    }
    return boxes;
  }

  _letterBox(LetterData letter){
    Color color = Colors.white;
    if(letter.state==LetterState.CORRECT){
      color = Colors.lightGreen;
    }
    if(letter.state==LetterState.WRONG){
      color = Colors.grey;
    }
    if(letter.state==LetterState.WRONG_POSITION) color = Colors.orangeAccent;
    return Container(
      width: 50,
      height: 50,
      padding: EdgeInsets.all(5),
      child: Container(
        decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(4),
            border: Border.all(color: Colors.grey)
        ),
        child: Center(
          child: Text(letter.content, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
        ),

      ),

    );
  }


  
}