
import 'package:sqflite/sqflite.dart';

class Application{
  static final Application _application = Application._internal();

  Database db;
  factory Application(){
    return _application;
  }
  Application._internal();
}

Application app = Application();