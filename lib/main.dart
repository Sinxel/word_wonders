import 'package:flutter/material.dart';
import 'package:word_wonders/pages/home_page.dart';
import 'package:word_wonders/values/app_colors.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  runApp(WordWonders());
}

class WordWonders extends StatefulWidget{
  _WordWondersState createState() => _WordWondersState();
}

class _WordWondersState extends State<WordWonders>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Word Wonders",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        canvasColor: Color(AppColors.canvas),
        primaryColor: Color(AppColors.primary),
        accentColor: Color(AppColors.accent)

      ),
      home: HomePage(),
    );
  }
  
}
