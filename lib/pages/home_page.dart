import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:word_wonders/application.dart';
import 'package:word_wonders/pages/guess_game.dart';
import 'package:word_wonders/pages/pattern_page.dart';
import 'package:word_wonders/pages/unscramble_page.dart';
import 'package:word_wonders/values/app_colors.dart';

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isDataLoaded = false;
  String randomWord = "";

  TextEditingController _searchController = new TextEditingController();
  TextEditingController _patternController = new TextEditingController();

  @override
  void initState() {
    checkDatabase();
    super.initState();
  }

  checkDatabase() async {
    app.db = await openDatabase(
      path.join(await getDatabasesPath(), "wordwonders.db"),
      onCreate: (db, version) {
        return db.execute(
            "CREATE TABLE english (id INTEGER PRIMARY KEY, word TEXT)");
      },
      version: 1,
    );
    String sql = "SELECT COUNT(word) as total FROM english;";
    List<Map> result = await app.db.rawQuery(sql);
    int totalWords = result[0]["total"];
    if (totalWords == 0) {
      _importWords();
    } else {
      _createGuessTables();
    }
  }

  _createGuessTables() async{
    String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='guessedWords'";
    List<Map> result = await app.db.rawQuery(sql);
    if(result.length==0){
      sql = "CREATE TABLE guessedWords (word TEXT);";
      await app.db.execute(sql);
      sql = "CREATE TABLE guessGames (submittedWord TEXT, correctAnswer INTEGER, answerOrder INT);";
      await app.db.execute(sql);
    }
    result.forEach((element) {
      print("el" + element.keys.length.toString());
    });
    setState(() {
      _isDataLoaded = true;
    });
  }
  _importWords() async {
    String data = await DefaultAssetBundle.of(context)
        .loadString("assets/raw/words.json");

    final Map<String, dynamic> words = json.decode(data);
    int id = 0;
    String sql = "INSERT INTO english (word) VALUES(?);";
    var batch = app.db.batch();
    int total = words.length;
    words.forEach((key, value) async {
      batch.rawInsert(sql, [key]);

      id++;
    });
    await batch.commit(noResult: true);
    sql = "SELECT COUNT(word) as total FROM english;";
    List<Map> result = await app.db.rawQuery(sql);


    _createGuessTables();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Word Wonders"),
      ),
      body: _buildPage(),
    );
  }

  _buildPage() {
    if (!_isDataLoaded) {
      return Container(
        padding: EdgeInsets.all(30),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Color(0xFFFFFFFF)),
              ),
              SizedBox(
                height: 20,
              ),
              Text("Preparing Database"),
            ],
          ),
        ),
      );
    }
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: ListView(
          children: [
            _gamesWidget(),
            SizedBox(
              height: 20,
            ),
            _unscrambleWidget(),
            SizedBox(
              height: 20,
            ),
            _patternWidget(),
          ],
        ),
      ),
    );
  }

  _gamesWidget() {
    return ExpansionTile(
      title: Text("Games"),
      childrenPadding: EdgeInsets.all(20),
      initiallyExpanded: true,
      children: [
        TextButton(
          onPressed: () {
            Navigator.of(context).push(new MaterialPageRoute(builder: (context)=>GuessGamePage()));
          },
          child: Text("Guess The Word"),
        )
      ],
    );
  }

  _patternWidget() {
    return ExpansionTile(
      title: Text("Find a pattern"),
      childrenPadding: EdgeInsets.all(20),
      initiallyExpanded: true,
      children: [
        TextField(
          decoration: InputDecoration(
            fillColor: Color(AppColors.inputBackground),
            filled: true,
            border: InputBorder.none,
            hintText: "Pattern",
          ),
          controller: _patternController,
          autocorrect: false,
          enableSuggestions: false,
          keyboardType: TextInputType.visiblePassword,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "Type the pattern and replace unknown letters with a question mark (?) for example, type '?o?e' to find all words that has 4 letters with O as the 2nd letter and E as the 4th.",
          style: TextStyle(fontSize: 10, color: Colors.black45),
        ),
        SizedBox(
          height: 10,
        ),
        TextButton(
          onPressed: () {
            if (_patternController.text.trim() == "") return;
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => PatternPage(_patternController.text)));
          },
          child: Row(
            children: [
              Icon(MdiIcons.magnify),
              SizedBox(
                width: 10,
              ),
              Text("Find"),
            ],
          ),
        )
      ],
    );
  }

  _unscrambleWidget() {
    return ExpansionTile(
      title: Text("Unscramble"),
      childrenPadding: EdgeInsets.all(20),
      initiallyExpanded: true,
      children: [
        TextField(
          decoration: InputDecoration(
            fillColor: Color(AppColors.inputBackground),
            filled: true,
            border: InputBorder.none,
            hintText: "Scrambled Word",
          ),
          controller: _searchController,
          autocorrect: false,
          enableSuggestions: false,
          keyboardType: TextInputType.visiblePassword,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          "Type the scrambled word then tap Unscramble to get a list of possible unscrambled words",
          style: TextStyle(fontSize: 10, color: Colors.black45),
        ),
        SizedBox(
          height: 10,
        ),
        TextButton(
          onPressed: () {
            if (_searchController.text.trim() == "") return;
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => UnscramblePage(_searchController.text)));
          },
          child: Row(
            children: [
              Icon(MdiIcons.diceMultiple),
              SizedBox(
                width: 10,
              ),
              Text("Unscramble"),
            ],
          ),
        )
      ],
    );
  }
}
