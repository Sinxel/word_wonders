enum LetterState{
  NONE,
  WRONG,
  WRONG_POSITION,
  CORRECT
}
class LetterData{
  LetterState state;
  String content;
  LetterData({this.state=LetterState.NONE, this.content = ""});
}