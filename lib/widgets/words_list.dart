import 'package:flutter/material.dart';
import 'package:word_wonders/application.dart';

class WordsList extends StatefulWidget{
  final int numberOfLetters;
  final String letters;
  WordsList(this.numberOfLetters, this.letters);
  _WordsListState createState() => _WordsListState();
}

class _WordsListState extends State<WordsList>{
  String list;

  @override
  void initState() {
    _generateList();
    // TODO: implement initState
    super.initState();
  }

  _generateList() async{
    String sql = "SELECT word FROM english WHERE length(word) = " + widget.numberOfLetters.toString();
    Map<String, int> letters = new Map();
    String pool = "abcdefghijklmnopqrstuvwxyz";
    pool.characters.forEach((c) {
      letters[c] = c.allMatches(widget.letters).length;
    });

    letters.forEach((key, value) {
      sql += " AND length(word) - length(replace(word, '"+key+"', ''))  BETWEEN 0 AND " + value.toString() ;
    });
    sql += " ORDER BY word";
    List<Map> results = await app.db.rawQuery(sql);
    List<String> words = [];
    print(results.length);
    results.forEach((element) {
      words.add(element["word"]);
    });
    String calculated = "";
    if(words.length==0){
      calculated = "No words found";
    }else{
      calculated = words.join(", ");
    }
    setState(() {

      list = calculated;
    });
    return ;
  }
  @override
  Widget build(BuildContext context) {
    return list==null?Center(child: CircularProgressIndicator(),):Text(list);
  }

}