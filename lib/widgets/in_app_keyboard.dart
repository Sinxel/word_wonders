import 'package:flutter/material.dart';
import 'package:word_wonders/widgets/button_key.dart';

class InAppKeyboard extends StatelessWidget{
   InAppKeyboard({
    Key key,
    this.keyPressed,
  }):super(key: key);
  OnKey keyPressed;
  @override
  Widget build(BuildContext context) {

    return Container(


      child: Column(
        children: [
          _buildTopRow(),
          _buildMiddleRow(),
          _buildBottomRow(),
          _buildActionsRow(),
        ],
      ),
    );
  }

  _buildTopRow(){
    String keysText = "qwertyuiop";
    List<Widget> keys = [];
    keysText.characters.forEach((c) {
      keys.add(ButtonKey(character: c.toUpperCase(), callBack: keyPressed,));
    });
    return Row(
      children: keys,
    );
  }

  _buildMiddleRow(){
    String keysText = "asdfghjkl";
    List<Widget> keys = [];
    keysText.characters.forEach((c) {
      keys.add(ButtonKey(character: c.toUpperCase(), callBack: keyPressed));
    });
    return Row(
      children: keys,
    );
  }
  _buildBottomRow(){
    String keysText = "zxcvbnm";
    List<Widget> keys = [];
    keysText.characters.forEach((c) {
      keys.add(ButtonKey(character: c.toUpperCase(), callBack: keyPressed));
    });
    return Row(
      children: keys,
    );
  }

  _buildActionsRow(){
    List<Widget> keys = [];
    keys.add(ButtonKey(character: "DELETE", color: Colors.redAccent,callBack: keyPressed));
    keys.add(ButtonKey(character: "ENTER", color: Colors.greenAccent, callBack: keyPressed));
    return  Row(
      children: keys,
    );
  }
  
}