import 'package:flutter/material.dart';

typedef OnKey(String content);

class ButtonKey extends StatelessWidget {
   Color color;
   OnKey callBack;
   ButtonKey({
    Key key,
    @required this.character,
     this.callBack,
     this.color = const Color(0xFF94d0ff)
  }) : super(key: key);
  final String character;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Padding(
        padding: EdgeInsets.all(2),
        child: Material(

          color: color,
          child: InkWell(
            onTap: (){
              if(callBack==null) return;
              callBack(character);
            },
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text(character),
              ),
            ),
          ),

        ),
      ),
    );
  }
}
